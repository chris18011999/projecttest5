import { Projecttest5Page } from './app.po';

describe('projecttest5 App', () => {
  let page: Projecttest5Page;

  beforeEach(() => {
    page = new Projecttest5Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
