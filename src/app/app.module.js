System.register(["@angular/core", "@angular/platform-browser", "./app.routing", "./app.component", "./components/home/home.component", "./components/404/error.component", "./components/dashboard/dashboard.component", "./components/login/login.component", "./components/register/register.component", "./components/settings/settings.component", "./components/resetpass/resetpass.component", "./components/feedback/feedback.component", "./services/auth.service", "./auth.guard"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, platform_browser_1, app_routing_1, app_component_1, home_component_1, error_component_1, dashboard_component_1, login_component_1, register_component_1, settings_component_1, resetpass_component_1, feedback_component_1, auth_service_1, auth_guard_1, AppModule;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (platform_browser_1_1) {
                platform_browser_1 = platform_browser_1_1;
            },
            function (app_routing_1_1) {
                app_routing_1 = app_routing_1_1;
            },
            function (app_component_1_1) {
                app_component_1 = app_component_1_1;
            },
            function (home_component_1_1) {
                home_component_1 = home_component_1_1;
            },
            function (error_component_1_1) {
                error_component_1 = error_component_1_1;
            },
            function (dashboard_component_1_1) {
                dashboard_component_1 = dashboard_component_1_1;
            },
            function (login_component_1_1) {
                login_component_1 = login_component_1_1;
            },
            function (register_component_1_1) {
                register_component_1 = register_component_1_1;
            },
            function (settings_component_1_1) {
                settings_component_1 = settings_component_1_1;
            },
            function (resetpass_component_1_1) {
                resetpass_component_1 = resetpass_component_1_1;
            },
            function (feedback_component_1_1) {
                feedback_component_1 = feedback_component_1_1;
            },
            function (auth_service_1_1) {
                auth_service_1 = auth_service_1_1;
            },
            function (auth_guard_1_1) {
                auth_guard_1 = auth_guard_1_1;
            }
        ],
        execute: function () {
            AppModule = (function () {
                function AppModule() {
                }
                return AppModule;
            }());
            AppModule = __decorate([
                core_1.NgModule({
                    imports: [platform_browser_1.BrowserModule, app_routing_1.routing],
                    declarations: [
                        app_component_1.AppComponent,
                        home_component_1.HomeComponent,
                        error_component_1.ErrorComponent,
                        dashboard_component_1.DashboardComponent,
                        login_component_1.LoginComponent,
                        register_component_1.RegisterComponent,
                        settings_component_1.SettingsComponent,
                        resetpass_component_1.ResetpassComponent,
                        feedback_component_1.FeedbackComponent
                    ],
                    bootstrap: [app_component_1.AppComponent],
                    providers: [app_routing_1.appRoutingProviders, auth_service_1.Auth, auth_guard_1.AuthGuard]
                })
            ], AppModule);
            exports_1("AppModule", AppModule);
        }
    };
});
//# sourceMappingURL=app.module.js.map