import {ModuleWithProviders} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { ErrorComponent } from './components/404/error.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { SettingsComponent } from './components/settings/settings.component';
import { ResetpassComponent } from './components/resetpass/resetpass.component';
import { FeedbackComponent } from './components/feedback/feedback.component';
import { CompanyComponent } from './components/register/company/company.component';
 
const appRoutes: Routes = [
  { path: '', component: HomeComponent },
  { path: '404', component: ErrorComponent },
  { path: 'dashboard', component: DashboardComponent},
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'settings', component: SettingsComponent},
  { path: 'resetpass', component: ResetpassComponent},
  { path: 'feedback', component: FeedbackComponent},
  { path: 'register/company', component: CompanyComponent },
  { path: '**', redirectTo: '/404' }
];

export const appRoutingProviders: any[] = [];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
