import { Component } from "@angular/core";
import { Router } from '@angular/router';


@Component({
  selector: 'my-app',
  styleUrls:['css/styles.css'],
  templateUrl: 'app.component.html'
})
export class AppComponent {

	title:string = ""; 
}
