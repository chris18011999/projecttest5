import { Component } from "@angular/core";
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { Http } from '@angular/http';


@Component({
  selector: 'login-page',
  styleUrls: ['../../css/style.css'],
  templateUrl: './login.component.html'
})

export class LoginComponent {
    private http: Http;

	title = "ByCycling Login";
	rForm: FormGroup;
	body: any;
	email:string = '';
	password:string = '';
	public router : Router;

	constructor(private fb: FormBuilder){

		this.rForm = fb.group({
			'email':[null, Validators.required],
			'password':[null, Validators.required]
		})
	}

	senddata(router : Router, http: Http) {

	var email = this.rForm.get('email').value;
	var password = this.rForm.get('password').value;

	var request = new XMLHttpRequest();

	request.open('POST', 'http://54.191.0.71/business/login');	

	request.setRequestHeader('Content-Type', 'application/json');

	request.onreadystatechange = function () {
	  if (this.readyState === 4) {
	    console.log('Status:', this.status);
	    console.log('Headers:', this.getAllResponseHeaders());
	    console.log('Body:', this.responseText);
	    console.log('Token: ', this.responseText);
	  }
	};

	var bodysend = {
	    "email": email,
	    "password": password
	    };

	var token = request.getAllResponseHeaders(); 
	console.log(JSON.stringify(bodysend));
	request.send(JSON.stringify(bodysend));
	console.log(token);
}

}

