import { Component } from "@angular/core";
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';


@Component({
  selector: 'register-page',
  styleUrls: ['../../css/style.css'],
  templateUrl: 'register.component.html'
})

export class RegisterComponent {

	rForm: FormGroup;
	body: any;
	companyID:number;
	username:string = '';
	email:string = '';
	password:string = '';
 
 constructor(private fb: FormBuilder){

		this.rForm = fb.group({
			'companyID':[null, Validators.required],
			'username':[null, Validators.required],
			'email':[null, Validators.required],
			'password':[null, Validators.required]
		})

	}

sendRegisterData(){
	var companyID = this.rForm.get('companyID').value;
	var email = this.rForm.get('email').value;
	var username = this.rForm.get('username').value;
	var password = this.rForm.get('password').value;

	var request = new XMLHttpRequest();

request.open('POST', 'http://54.191.0.71/business/admins');

request.setRequestHeader('Content-Type', 'application/json');
request.setRequestHeader('Authorization', 'bearer eyJ0eXAiOiJKssfdhDSG346JhbGciOiJIUzI1NiJ9.eyJhZG1pbiI6dHJ1ZSwiaWQiSgfdssfgEGS45LCJpYXQiOjE0OTA4NzYyNzF9.tlXeKSKYa9n-Bvwf-igbajzpB-VMIU996BsuBRVlnJU');


request.onreadystatechange = function () {
  if (this.readyState === 4) {
    console.log('Status:', this.status);
    console.log('Headers:', this.getAllResponseHeaders());
    console.log('Body:', this.responseText);
  }
};

var body = {

  'companyID': companyID,
  'email': email,
  'username': username,
  'password': password,

};

console.log(body);
request.send(JSON.stringify(body));
 }
}

