import { Component } from "@angular/core";
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';
@Component({
  selector: 'company-page',
  styleUrls: ['../../../css/style.css'],
  templateUrl: 'company.component.html'
})
export class CompanyComponent {
    rForm: FormGroup;
    body: any;

    name:string = '';
    tier:number;
    isPartner:number;
    passphrase:string = '';
    domain:string = '';
    validStartHour:string = '';
    validEndHour:string = '';
    validStartDay:number;
    validEndDay:number;
    reportStartDay:string = '';
    budget:number;
    averageCommuteDistance:number;
    username:string = '';
    email:string = '';
    password:string = '';
 
 constructor(private fb: FormBuilder){
        this.rForm = fb.group({
            'name':[null, Validators.required],
            'tier':[null, Validators.required],
            'isPartner':[null, Validators.required],
            'passphrase':[null, Validators.required],
            'domain':[null, Validators.required],
            'validStartHour':[null, Validators.required],
            'validEndHour':[null, Validators.required],
            'validStartDay':[null, Validators.required],
            'validEndDay':[null, Validators.required],
            'reportStartDay':[null, Validators.required],
            'budget':[null, Validators.required],
            'averageCommuteDistance':[null, Validators.required],
            'username':[null, Validators.required],
            'email':[null, Validators.required],
            'password':[null, Validators.required]
        })
    }
sendRegisterData(){
    var name = this.rForm.get('name').value;
    var tier = this.rForm.get('tier').value;
    var isPartner = this.rForm.get('isPartner').value;
    var passphrase = this.rForm.get('passphrase').value;
    var domain = this.rForm.get('domain').value;
    var validStartHour = this.rForm.get('validStartHour').value;
    var validEndHour = this.rForm.get('validEndHour').value;
    var validStartDay = this.rForm.get('validStartDay').value;
    var validEndDay = this.rForm.get('validEndDay').value;
    var reportStartDay = this.rForm.get('reportStartDay').value;
    var budget = this.rForm.get('budget').value;
    var averageCommuteDistance = this.rForm.get('averageCommuteDistance').value;
    var username = this.rForm.get('username').value;
    var email = this.rForm.get('email').value;
    var password = this.rForm.get('password').value;

	var request = new XMLHttpRequest();
	request.open('POST', 'http://54.191.0.71/business/register');
	request.setRequestHeader('Content-Type', 'application/json');
	request.setRequestHeader('Authorization','bearer eyJ0eXAiOiJKssfdhDSG346JhbGciOiJIUzI1NiJ9.eyJhZG1pbiI6dHJ1ZSwiaWQiSgfdssfgEGS45LCJpYXQiOjE0OTA4NzYyNzF9.tlXeKSKYa9n-Bvwf-igbajzpB-VMIU996BsuBRVlnJU');
	request.onreadystatechange = function () {
  	if (this.readyState === 4) {
    console.log('Status:', this.status);
    console.log('Headers:', this.getAllResponseHeaders());
    console.log('Body:', this.responseText);
  }
};
var body = {
    'name': name,
    'tier': tier,
    'isPartner': isPartner,
    'passphrase': passphrase,
    'domain': domain,
    'validStartHour': validStartHour,
    'validEndHour': validEndHour,
    'validStartDay': validStartDay,
    'validEndDay': validEndDay,
    'reportStartDay': reportStartDay,
    'budget': budget,
    'averageCommuteDistance': averageCommuteDistance,
    'username': username,
    'email': email,
    'password': password
 };
console.log(body);
request.send(JSON.stringify(body));
 }
}
