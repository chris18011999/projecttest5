var signupOptions = {
  languageDictionary: {
    title: "Sign up for ByCycling"
  },
  rememberLastLogin: false,
  allowLogin: false,
  allowedConnections: ['Username-Password-Authentication'],
  theme: {
    logo: 'src/app/img/BC-favicon.png',
    primaryColor: '#16112d'
  }
};


var webAuth = new Auth0Lock('OUn8zuAhhr2IpC2lkegUHU6dK5czf5Ni', 'ginot.eu.auth0.com', signupOptions);
