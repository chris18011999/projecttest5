System.register(["@angular/router", "./auth.guard", "./components/home/home.component", "./components/404/error.component", "./components/dashboard/dashboard.component", "./components/login/login.component", "./components/register/register.component", "./components/settings/settings.component", "./components/resetpass/resetpass.component", "./components/feedback/feedback.component"], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var router_1, auth_guard_1, home_component_1, error_component_1, dashboard_component_1, login_component_1, register_component_1, settings_component_1, resetpass_component_1, feedback_component_1, appRoutes, appRoutingProviders, routing;
    return {
        setters: [
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (auth_guard_1_1) {
                auth_guard_1 = auth_guard_1_1;
            },
            function (home_component_1_1) {
                home_component_1 = home_component_1_1;
            },
            function (error_component_1_1) {
                error_component_1 = error_component_1_1;
            },
            function (dashboard_component_1_1) {
                dashboard_component_1 = dashboard_component_1_1;
            },
            function (login_component_1_1) {
                login_component_1 = login_component_1_1;
            },
            function (register_component_1_1) {
                register_component_1 = register_component_1_1;
            },
            function (settings_component_1_1) {
                settings_component_1 = settings_component_1_1;
            },
            function (resetpass_component_1_1) {
                resetpass_component_1 = resetpass_component_1_1;
            },
            function (feedback_component_1_1) {
                feedback_component_1 = feedback_component_1_1;
            }
        ],
        execute: function () {
            appRoutes = [
                { path: '', component: home_component_1.HomeComponent },
                { path: '404', component: error_component_1.ErrorComponent },
                { path: 'dashboard', component: dashboard_component_1.DashboardComponent, canActivate: [auth_guard_1.AuthGuard] },
                { path: 'login', component: login_component_1.LoginComponent },
                { path: 'register', component: register_component_1.RegisterComponent },
                { path: 'settings', component: settings_component_1.SettingsComponent, canActivate: [auth_guard_1.AuthGuard] },
                { path: 'resetpass', component: resetpass_component_1.ResetpassComponent, canActivate: [auth_guard_1.AuthGuard] },
                { path: 'feedback', component: feedback_component_1.FeedbackComponent, canActivate: [auth_guard_1.AuthGuard] },
                { path: '**', redirectTo: '/404' }
            ];
            exports_1("appRoutingProviders", appRoutingProviders = []);
            exports_1("routing", routing = router_1.RouterModule.forRoot(appRoutes));
        }
    };
});
//# sourceMappingURL=app.routing.js.map